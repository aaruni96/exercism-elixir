defmodule NameBadge do
  @moduledoc """
  This module implements a function to print a name badge for employees.
  """
  @doc """
  This function implements the printing of a name badge for employees.
  """
  @spec print(nil | integer(), name :: String.t(), department :: String.t())  ::  String.t()
  def print(id, name, department) do
    dep = if department == nil do
      "Owner"
    else
      department
    end
    if id == nil do
      "#{name} - #{String.upcase(dep)}"
    else
      "[#{id}] - #{name} - #{String.upcase(dep)}"
    end
  end
end
