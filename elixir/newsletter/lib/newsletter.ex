#i'm sleepy and skipping docs and specs
defmodule Newsletter do
  def read_emails(path) do
    {_status,content} = File.read(path)
    String.split(content)
  end

  def open_log(path) do
    File.open!(path, [:write] )
  end

  def log_sent_email(pid, email) do
    IO.puts(pid, email)
  end

  def close_log(pid) do
    File.close(pid)
  end

  def send_newsletter(emails_path, log_path, send_fun) do
    emails = read_emails(emails_path)
    log_device = open_log(log_path)

    for email <- emails do
      if send_fun.(email) == :ok do
        log_sent_email(log_device, email)
      end
    end

    close_log(log_device)
  end

end
