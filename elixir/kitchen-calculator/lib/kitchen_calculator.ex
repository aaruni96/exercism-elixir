defmodule KitchenCalculator do
  def get_volume(volume_pair) do
    # Please implement the get_volume/1 function
    elem(volume_pair,1)
  end

  def to_milliliter(volume_pair) do
    # Please implement the to_milliliter/1 functions
    multiplier = cond do
      elem(volume_pair,0) == :cup -> 240
      elem(volume_pair,0) == :fluid_ounce -> 30
      elem(volume_pair,0) == :teaspoon -> 5
      elem(volume_pair,0) == :tablespoon -> 15
      elem(volume_pair,0) == :milliliter -> 1
    end
    new_vol = multiplier * get_volume(volume_pair)
    {:milliliter, new_vol}
  end

  def from_milliliter(volume_pair, unit) do
    # Please implement the from_milliliter/2 functions
    multiplier = cond do
      unit == :cup -> 1/240
      unit == :fluid_ounce -> 1/30
      unit == :teaspoon -> 1/5
      unit == :tablespoon -> 1/15
      unit == :milliliter -> 1
    end
    new_vol = multiplier * get_volume(volume_pair)
    {unit, new_vol}
  end

  def convert(volume_pair, unit) do
    # Please implement the convert/2 function
    from_milliliter(to_milliliter(volume_pair), unit)
  end
end
