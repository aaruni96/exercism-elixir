# Use the Plot struct as it is provided
defmodule Plot do
  @enforce_keys [:plot_id, :registered_to]
  defstruct [:plot_id, :registered_to]
end

defmodule CommunityGarden do
  def start(opts \\ [] ) do
    Agent.start(fn -> {0,[]} end, opts)
  end

  def list_registrations(pid) do
    Agent.get(pid, fn {_state,list} -> list end)
  end

  def register(pid, register_to) do
    Agent.update(pid, fn {state,list} -> {state+1, [%Plot{plot_id: state+1, registered_to: register_to } | list]} end)
    hd(list_registrations(pid))
  end

  def release(pid, plot_id) do
    list = Enum.filter(list_registrations(pid), fn x -> x.plot_id != plot_id end)
    Agent.update(pid, fn {state, _} -> {state, list} end )
  end

  def get_registration(pid, plot_id) do
    list = Enum.filter(list_registrations(pid), fn x-> x.plot_id == plot_id end)
    if list == [] do
      {:not_found, "plot is unregistered"}
    else
      [head|_] = list
      head
    end
  end
end
