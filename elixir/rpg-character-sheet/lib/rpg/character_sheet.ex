defmodule RPG.CharacterSheet do
  @moduledoc """
  This module implements some functionality which helps in creating a character sheet.
  """

  @doc """
  Simple welcome message
  """
  @spec welcome() :: :ok
  def welcome() do
    IO.puts("Welcome! Let's fill out your character sheet together.")
  end

  @doc """
  Function echos name
  """
  @spec ask_name()  ::  String.t()
  def ask_name() do
    name = IO.gets("What is your character's name?\n")
    String.trim(name)
  end

  @doc """
  Function echos class
  """
  @spec ask_class() ::  String.t()
  def ask_class() do
    class = IO.gets("What is your character's class?\n")
    String.trim(class)
  end

  @doc """
  Function echos level
  """
  @spec ask_level() ::  integer()
  def ask_level() do
    lvl = IO.gets("What is your character's level?\n")
    String.to_integer(String.trim(lvl))
  end

  @doc """
  Combines functions of this module into a sequence.
  """
  @spec run() ::  map()
  def run() do
    welcome()
    name = ask_name()
    class = ask_class()
    lvl = ask_level()
    character = %{class: class, level: lvl, name: name}
    IO.inspect(character, label: "Your character")
  end
end
