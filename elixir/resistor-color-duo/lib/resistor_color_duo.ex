defmodule ResistorColorDuo do
  @doc """
  Calculate a resistance value from two colors
  """

  @translation_map %{black: 0, brown: 1, red: 2, orange: 3, yellow: 4, green: 5, blue: 6, violet: 7, grey: 8, white: 9}

  @spec value(colors :: [atom]) :: integer
  def value(colors) do
    [one, two | _tail ] = colors
    10*@translation_map[one] + @translation_map[two]
  end
end
