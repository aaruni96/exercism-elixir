defmodule DNA do

  @encode_map %{?\s => 0b0000, ?A => 0b0001, ?C => 0b0010, ?G => 0b0100, ?T => 0b1000}

  @decode_map %{0b0000 => ?\s, 0b0001 => ?A, 0b0010 => ?C, 0b0100 => ?G, 0b1000 => ?T}

  def encode_nucleotide(code_point) do
    # Please implement the encode_nucleotide/1 function
    @encode_map[code_point]
  end

  def decode_nucleotide(encoded_code) do
    # Please implement the decode_nucleotide/1 function
    @decode_map[encoded_code]
  end

  def encode(dna), do: do_encode(dna, <<0 :: size(0)>>)

  def decode(dna), do: do_decode(dna, [])

  defp do_encode([], encode), do: encode

  defp do_encode([head | tail], encode) do
    encode = << encode::bitstring, encode_nucleotide(head) :: size(4) >>
    do_encode(tail,encode )
  end

  defp do_decode("", decode), do: decode

  defp do_decode(<< head :: size(4), rest :: bitstring >>, decode) do
    decode = decode ++ [decode_nucleotide(head)]
    do_decode(rest, decode)
  end

end
