defmodule HighSchoolSweetheart do
  def first_letter(name) do
    # Please implement the first_letter/1 function
    String.first(String.trim(name))
  end

  def initial(name) do
    # Please implement the initial/1 function
    first_letter(name)
    |> String.capitalize()
    |>Kernel.<>(".")
  end

  def initials(full_name) do
    # Please implement the initials/1 function
    [firstname | [lastname | []]] = String.split(full_name)
    "#{initial(firstname)} #{initial(lastname)}"

  end

  def pair(full_name1, full_name2) do
    #      ******       ******
    #    **      **   **      **
    #  **         ** **         **
    # **            *            **
    # **                         **
    # **     X. X.  +  X. X.     **
    #  **                       **
    #    **                   **
    #      **               **
    #        **           **
    #          **       **
    #            **   **
    #              ***
    #               *

    # Please implement the pair/2 function
    """
     ******       ******
   **      **   **      **
 **         ** **         **
**            *            **
**                         **
**     #{initials(full_name1)}  +  #{initials(full_name2)}     **
 **                       **
   **                   **
     **               **
       **           **
         **       **
           **   **
             ***
              *
"""
  end
end
