defmodule RationalNumbers do
  @type rational :: {integer, integer}

  @doc """
  Add two rational numbers
  """
  @spec add(a :: rational, b :: rational) :: rational
  def add(a, b) do
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    b_num  = elem(b,0)
    b_denom = elem(b,1)
    return_num = (a_num*b_denom) + (b_num * a_denom)
    return_denom = (a_denom * b_denom)
    return = {return_num, return_denom}
    reduce(return)
  end

  @doc """
  Subtract two rational numbers
  """
  @spec subtract(a :: rational, b :: rational) :: rational
  def subtract(a, b) do
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    b_num  = elem(b,0)
    b_denom = elem(b,1)
    return_num = (a_num*b_denom) - (b_num * a_denom)
    return_denom = (a_denom * b_denom)
    return = {return_num, return_denom}
    reduce(return)
  end

  @doc """
  Multiply two rational numbers
  """
  @spec multiply(a :: rational, b :: rational) :: rational
  def multiply(a, b) do
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    b_num  = elem(b,0)
    b_denom = elem(b,1)
    return_num = (a_num*b_num)
    return_denom = (a_denom * b_denom)
    return = {return_num, return_denom}
    reduce(return)
  end

  @doc """
  Divide two rational numbers
  """
  @spec divide_by(num :: rational, den :: rational) :: rational
  def divide_by(num, den) do
    a_num  = elem(num,0)
    a_denom = elem(num,1)
    b_num  = elem(den,0)
    b_denom = elem(den ,1)
    return_num = (a_num*b_denom)
    return_denom = (a_denom * b_num)
    return = {return_num, return_denom}
    reduce(return)
  end

  @doc """
  Absolute value of a rational number
  """
  @spec abs(a :: rational) :: rational
  def abs(a) do
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    return_num = Kernel.abs(a_num)
    return_denom = Kernel.abs(a_denom)
    return = {return_num, return_denom}
    reduce(return)
  end

  @doc """
  Exponentiation of a rational number by an integer
  """
  @spec pow_rational(a :: rational, n :: integer) :: rational
  def pow_rational(a, n) do
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    m = Kernel.abs(n)
    return = if m==n do
      {a_num**m,a_denom**m}
    else
      {a_denom**m, a_num**m}
    end
    reduce(return)
  end

  @doc """
  Exponentiation of a real number by a rational number
  """
  @spec pow_real(x :: integer, n :: rational) :: float
  def pow_real(x, n) do
    x = x/1.0
    a = reduce(n)
    a_num  = elem(a,0)
    a_denom = elem(a,1)
    return_num = Float.pow(x,a_num)
    Float.pow(return_num, 1/a_denom)
  end

  @doc """
  Reduce a rational number to its lowest terms
  """
  @spec reduce(a :: rational) :: rational
  def reduce(a) do
    a_denom = elem(a,1)
    b = if (a_denom < 0) do
      multiply(a,{-1,-1})
    else
      a
    end
    #b = {1,1}
    a_num = elem(b,0)
    a_denom = elem(b,1)
    g = Integer.gcd(a_num, a_denom)
    {Kernel.trunc(a_num/g), Kernel.trunc(a_denom/g)}

  end
end
