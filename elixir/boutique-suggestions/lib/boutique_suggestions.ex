defmodule BoutiqueSuggestions do
  @moduledoc """
  Suggests item combinations for a boutique
  """

  @doc """
  Gets "reasonable combinations" from boutique selection
  """
  @spec get_combinations(tops::list(map()), bottoms::list(map()), options::list(keyword())) ::  list(map())
  def get_combinations(tops, bottoms, options \\ []) do
    maxval = Keyword.get(options, :maximum_price, 100)
    for top <- tops,
    bottom <- bottoms,
    top.base_color != bottom.base_color,
    (top.price + bottom.price) <= maxval do
      {top, bottom}
    end
  end
end
