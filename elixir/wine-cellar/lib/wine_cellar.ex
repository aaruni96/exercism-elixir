defmodule WineCellar do

  @doc """
  Returns a keyword lits of wine colors and explanations
  """
  @spec explain_colors()  ::  keyword()
  def explain_colors do
    [white: "Fermented without skin contact.", red: "Fermented with skin contact using dark-colored grapes.", rose: "Fermented with some skin contact, but not enough to qualify as a red wine." ]
  end

  @doc """
  Takes a keyword list of wines, color, and options.
  What do the options do?
  Returns all of the wines of that color
  """
  @spec filter( keyword(), atom(), keyword() ) ::  list()

  def filter(cellar, color, opts \\ [])

  def filter(cellar, color, opts) when opts == [] do
    Keyword.get_values(cellar, color)
  end

  def filter(cellar, color, opts) do

    new_cellar = Keyword.get_values(cellar, color)
    opts_list = Keyword.keys(opts)
    new_cellar = if :year in opts_list do
      filter_by_year(new_cellar, opts[:year])
    else
      new_cellar
    end

    if :country in opts_list do
      filter_by_country(new_cellar, opts[:country])
    else
      new_cellar
    end

  end

  # The functions below do not need to be modified.

  defp filter_by_year(wines, year)
  defp filter_by_year([], _year), do: []

  defp filter_by_year([{_, year, _} = wine | tail], year) do
    [wine | filter_by_year(tail, year)]
  end

  defp filter_by_year([{_, _, _} | tail], year) do
    filter_by_year(tail, year)
  end

  defp filter_by_country(wines, country)
  defp filter_by_country([], _country), do: []

  defp filter_by_country([{_, _, country} = wine | tail], country) do
    [wine | filter_by_country(tail, country)]
  end

  defp filter_by_country([{_, _, _} | tail], country) do
    filter_by_country(tail, country)
  end
end
