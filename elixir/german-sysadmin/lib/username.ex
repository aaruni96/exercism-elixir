defmodule Username do
  @moduledoc """
  Module implements username sanitaztion function
  """
  @translation_map %{?ä=>[?a,?e], ?ö =>[?o,?e], ?ü => [?u,?e], ?ß => [?s,?s]}

  @doc """
  Function returns a sanitized username

  This process will get rid of numbers, and replace non-latin characters with latin approximates
  """
  @spec sanitize(username :: charlist()) :: charlist()

  def sanitize(''), do: ''

  def sanitize(username) do
    [head | tail ] = username
    case head do
      head when (head == ?_) -> [?_ | sanitize(tail)]
      head when (head in [?ä,?ö,?ü,?ß]) -> Map.get(@translation_map, head) ++ sanitize(tail)
      head when (head >= ?a and head <= ?z ) -> [head | sanitize(tail)]
      _ ->  '' ++ sanitize(tail)
    end
  end
end
