defmodule HighScore do

  @initial_score 0

  def new() do
    #new/0 returns an empty map
    %{}
  end

  def add_player(scores, name, score \\ @initial_score) do
    # add_player/3 adds player and score to map
    Map.put(scores, name, score)

  end

  def remove_player(scores, name) do
    # remove_player/2 removes a name and score from mape
    Map.delete(scores,name)
  end

  def reset_score(scores, name) do
    # reset_score/2 resets the score of name in scores to 0
    if name in get_players(scores) do
      %{scores | name => @initial_score}
    else
      add_player(scores, name)
    end
  end

  def update_score(scores, name, score) do
    # update_score/3 adds the score to the oldscore of name in scores
    Map.update(scores, name, score, fn value -> value+score end  )
  end

  def get_players(scores) do
    # get_players/1 gets all the player names in scores
    Map.keys(scores)
  end
end
