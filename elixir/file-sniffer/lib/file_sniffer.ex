defmodule FileSniffer do

  @moduledoc """
  Implements some methods for sniffing file mimetypes
  """

  @mime_map %{"exe" => "application/octet-stream", "bmp" => "image/bmp", "png" => "image/png", "jpg" => "image/jpg", "gif" => "image/gif"}


  @doc """
  Method takes an extension as input and returns mimetype
  """
  @spec type_from_extension(String.t())  ::  String.t()
  def type_from_extension(extension) do
    @mime_map[extension]
  end

  @doc """
  Method takes a binary file and returns mimetype
  """
  @spec type_from_binary(binary())  ::  String.t()
  def type_from_binary(file_binary) do
    <<magic :: binary-size(1), _::binary>> = file_binary
    cond do
       magic == <<0x7F>> and String.length(file_binary) > 4 ->
        <<magic :: binary-size(4), _ :: binary>> = file_binary
        magic
       magic == <<0x42>> and String.length(file_binary) > 2->
        <<magic :: binary-size(2), _ :: binary>> = file_binary
        magic
       magic == <<0x89>> and String.length(file_binary) > 8 ->
        <<magic :: binary-size(8), _ :: binary>> = file_binary
        magic
       magic == <<0xFF>> and String.length(file_binary) > 3 ->
        <<magic :: binary-size(3), _ :: binary>> = file_binary
        magic
       magic == <<0x47>> and String.length(file_binary) > 3 ->
        <<magic :: binary-size(3), _ :: binary>> = file_binary
        magic
       true -> nil
    end
    |> case do
      <<0x7F, 0x45, 0x4C, 0x46>> -> "exe"
      <<0x42, 0x4D>> -> "bmp"
      <<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A>> -> "png"
      <<0xFF, 0xD8, 0xFF>> -> "jpg"
      <<0x47, 0x49, 0x46>> -> "gif"
      _ -> nil
    end
    |> type_from_extension()
  end

  @doc """
  Method verifies that binary file has the right extension
  """
  @spec verify(binary(), String.t())  ::  tuple()
  def verify(file_binary, extension) do
    if type_from_binary(file_binary) == type_from_extension(extension) do
      {:ok, type_from_extension(extension) }
    else
      {:error, "Warning, file format and file extension do not match."}
    end
  end
end
