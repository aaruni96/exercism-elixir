defmodule RemoteControlCar do
  @moduledoc """
  Module implements a remote control car using a struct
  """
  @enforce_keys [:nickname]
  defstruct [:nickname, battery_percentage: 100, distance_driven_in_meters: 0]

  @doc """
  Creates a new car with a given nickname
  """
  @spec new(String.t()) ::  struct()
  def new(nickname \\ "none") do
    %RemoteControlCar{nickname: nickname}
  end

  @doc """
  Function displays the distance driven by the car
  """
  @spec display_distance(struct())  ::  String.t()
  def display_distance(remote_car) when remote_car.__struct__ == RemoteControlCar do
    distance = remote_car.distance_driven_in_meters
    "#{distance} meters"
  end

  @doc """
  Function displays the battery remaining in the car
  """
  @spec display_battery(struct()) ::  String.t()
  def display_battery(remote_car) when remote_car.__struct__ == RemoteControlCar do
    battery = remote_car.battery_percentage
    if(battery == 0) do
      "Battery empty"
    else
      "Battery at #{battery}%"
    end
  end

  @doc """
  Drives the remote car by 20 units, consumes 1 battery
  """
  @spec drive(struct()) ::  struct()

  def drive(remote_car) when remote_car.__struct__ == RemoteControlCar and remote_car.battery_percentage != 0 do
    distance = remote_car.distance_driven_in_meters + 20
    battery = remote_car.battery_percentage - 1
    %RemoteControlCar{remote_car | battery_percentage: battery, distance_driven_in_meters: distance}
  end

  def drive(remote_car) when remote_car.__struct__ == RemoteControlCar do
    remote_car
  end
end
