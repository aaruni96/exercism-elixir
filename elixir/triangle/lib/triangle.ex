defmodule Triangle do
  @type kind :: :equilateral | :isosceles | :scalene

  @doc """
  Return the kind of triangle of a triangle with 'a', 'b' and 'c' as lengths.
  """
  @spec kind(number, number, number) :: {:ok, kind} | {:error, String.t()}
  def kind(a, b, c) do

    if check_consistence(a,b,c) and check_positive(a,b,c) do
      {:ok,check_type(a,b,c)}
    else
      if not check_positive(a,b,c) do
        {:error, "all side lengths must be positive"}
      else
        {:error, "side lengths violate triangle inequality"}
      end
    end

  end

  #private functions begin

  defp check_consistence(a, b, c) do
    if (a+b) >= c and (b+c) >= a and (a+c) >= b do
      true
    else
      false
    end
  end

  defp check_positive(a,b,c) do
    a > 0 and b > 0 and c > 0
  end

  defp check_type(a,b,c) do

    if a==b and b==c do
      :equilateral
    else
      if a == b or a==c or b==c do
        :isosceles
      else
        :scalene
      end
    end

  end

end
