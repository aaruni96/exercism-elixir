defmodule LibraryFees do

  def datetime_from_string(string) do
    datetime = NaiveDateTime.from_iso8601(string)
    elem(datetime, 1)
  end

  def before_noon?(datetime) do
    datetime.hour < 12
  end

  def return_date(checkout_datetime) do
    days = if before_noon?(checkout_datetime), do: 28, else: 29

    NaiveDateTime.add(checkout_datetime, days, :day)
    |> NaiveDateTime.to_date()

  end

  def days_late(planned_return_date, actual_return_datetime) do

    ard = NaiveDateTime.to_date(actual_return_datetime)

    if Date.diff(ard,planned_return_date) < 0, do: 0, else: Date.diff(ard,planned_return_date)

  end

  def monday?(datetime) do
    NaiveDateTime.to_date(datetime)
    |> Date.day_of_week() == 1
  end

  def calculate_late_fee(checkout, return, rate) do
    checkout = datetime_from_string(checkout)
    return = datetime_from_string(return)
    arate = if monday?(return), do: rate/2, else: rate
    planned = return_date(checkout)
    trunc(arate * days_late(planned, return))

  end
end
