defmodule BoutiqueInventory do
  @moduledoc """
  Module provides methods to manage boutique inventory
  """

  @doc """
  Sorts given inventory by price
  """
  @spec sort_by_price(map())  :: map()
  def sort_by_price(inventory) do
    Enum.sort_by(inventory, &(&1.price))
  end

  @doc """
  Filters the inventory to return only items with missing prices
  """
  @spec with_missing_price(map()) ::  map()
  def with_missing_price(inventory) do
    Enum.filter(inventory, fn x -> x.price == nil end)
  end

  @doc """
  Updates names of items in inventory
  """
  @spec update_names(map(), String.t(), String.t()) ::  map()
  def update_names(inventory, old_word, new_word) do
    Enum.map(inventory, fn item ->
      name = String.replace(item.name, old_word, new_word)
      %{item | name: name} end)
  end

  @doc """
  Increases quantities of all sizes of inventory by count
  """
  @spec increase_quantity(map(), integer()) :: map()
  def increase_quantity(item, count) do
    # Please implement the increase_quantity/2 function
    newprice = Map.new(Enum.map(item.quantity_by_size, fn {s,n} -> {s,n+count} end))
    Map.new(name: item.name, price: item.price,quantity_by_size: newprice )
  end

  @doc """
  Returns the total quantity over all sizes of an item in inventory
  """
  @spec total_quantity(map()) ::  integer()
  def total_quantity(item) do
    # Please implement the total_quantity/1 function
    Enum.reduce(item.quantity_by_size, 0, fn ({x,i},count) -> i+count end)
  end
end
