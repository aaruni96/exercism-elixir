defmodule Gigasecond do
  @doc """
  Calculate a date one billion seconds after an input date.
  """
  @spec from({{pos_integer, pos_integer, pos_integer}, {pos_integer, pos_integer, pos_integer}}) ::
          {{pos_integer, pos_integer, pos_integer}, {pos_integer, pos_integer, pos_integer}}
  def from(erldate = {{_year, _month, _day}, {_hours, _minutes, _seconds}}) do

    stamp = NaiveDateTime.from_erl!(erldate)

    NaiveDateTime.add(stamp, 10**9)

    |> NaiveDateTime.to_erl()

  end
end
