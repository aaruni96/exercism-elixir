defmodule BirdCount do
  def today(list) do
    if list == [] do
      nil
    else
      hd list
    end
  end

  def increment_day_count(list) do
    head = if list == [] do
      1
    else
      (hd list) + 1
    end
    if length(list) < 2 do
      [head]
    else
      [_ | tail] = list
      [head | tail]
    end
  end

  def has_day_without_birds?(list) do
    0 in list
  end

  def total([]), do: 0

  def total([head | tail]) do
    head + total(tail)
  end

  def busy_days([]), do: 0

  def busy_days([head | tail]) do
    if head >=5 do
      1 + busy_days(tail)
    else
      busy_days(tail)
    end
  end
end
