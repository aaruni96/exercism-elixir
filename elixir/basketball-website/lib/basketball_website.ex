defmodule BasketballWebsite do

  def extract_from_path(data, path) do
    path = String.split(path, ".")
    do_extract_from_path(data, path)
  end

  def get_in_path(data, path) do
    get_in(data, String.split(path, "."))
  end

  defp do_extract_from_path(data, ''), do: data

  defp do_extract_from_path(data, path) do
    [head | tail] = path
    do_extract_from_path(data[head], tail)

  end


end
