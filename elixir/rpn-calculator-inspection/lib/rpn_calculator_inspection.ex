defmodule RPNCalculatorInspection do

  def start_reliability_check(calculator, input) do
    {_,pid} = Task.start_link(fn -> calculator.(input) end)
    %{input: input, pid: pid}
  end

  def await_reliability_check_result(%{pid: pid, input: input}, results) do
    receive do
      {:EXIT, from, reason} when reason==:normal and from == pid ->Map.merge(results, %{input => :ok})
      {:EXIT, from, _} when from == pid-> Map.merge(results, %{input => :error})
    after
      100 -> Map.merge(results, %{input => :timeout})
    end
  end

  def reliability_check(calculator, inputs) do
    old = Process.flag(:trap_exit, true)
    checks = Enum.map(inputs, fn input -> start_reliability_check(calculator, input) end)
    results = Enum.reduce(checks, %{}, fn check, results -> await_reliability_check_result(check, results) end)
    Process.flag(:trap_exit, old)
    results
  end

  def correctness_check(calculator, inputs) do
    Enum.map(inputs, fn input -> Task.async(fn -> calculator.(input) end) end)
    |> Enum.map(fn task -> Task.await(task, 100) end)
  end
end
