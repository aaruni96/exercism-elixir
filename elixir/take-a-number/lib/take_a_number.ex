defmodule TakeANumber do
  def start() do
    spawn(fn -> loop(0) end)

  end

  defp loop(state) do
    new_state = receive do
      {:report_state, sender_pid} ->
        send(sender_pid, state)
        state
      {:take_a_number, sender_pid} ->
        send(sender_pid, state+1)
        state+1
      :stop -> nil
      _ -> state
    end

    if new_state, do: loop(new_state)

  end

end
